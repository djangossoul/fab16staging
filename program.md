---
layout: page
title: Program
lang: en
permalink: /program/

programtitle: PROGRAM
programsubtitle: ""
programimg: ""

title_text: "PROGRAM"
subtitle_text: ""
color_class: "colorgreen"
img: ""

programdesc: "<strong style='font-size:22px;'>Be part of the largest conference on digital fabrication in the world.</strong>
                 <br><br>
                 Join us in Montréal, Canada, for the annual meeting of the international Fab Lab network!
                <br><br>
                Join world-class panelists, members of the Fab Lab community, makers from all over the world and professionals from all kinds of fields in the 16th annual Fab Lab Conference and the Fab Festival in Montréal, Canada, from July 27 to August 2, combined with the Fab City Summit, from 31 July to 2 August. Attend world-class panels, demos, workshops, discussions and lab presentations from representatives from all over the globe!
                <br><br>
                <strong style='font-size:22px;'>A preview of the upcoming program:</strong>
                <br><br>
                Conference on the state of the Fab Labs network and the impacts of the Fab Labs movement.<br>
                Prospective conferences<br>
                <ul>
                    <li>Latest digital manufacturing and machining methods</li>
                    <li>Innovative approaches at the level of materials and matter</li>
                    <li>Speakers and panelists on the theme and topics of the FAB16</li>
                </ul>
                Hands-on and theoretical workshops<br>
                Working groups<br>
                <ul>
                    <li>Academany Instructors</li>
                    <li>Assistive technologies</li>
                    <li>Humanitarian Lab</li>
                    <li>Scopes</li>
                    <li>Regional Networks</li>
                    <li>STEAM Education</li>
                    <li>Fab Labs with impact</li>
                    <li>Fab Kids</li>
                </ul>
                Research presentations<br>
                Challenges<br>
                Networking highlights<br>
                The Academany graduation ceremony<br>
                The “7th Digital Fashion and Wearables” exhibition<br>
                A SuperFabLab<br>
                A vast space equipped with all the equipment of a Fab Lab (and much more)<br>
                Mobile Fab Labs<br>
                <br><br>
                <strong style='font-size:22px;'>Fab Festival - August 1st and 2nd</strong>
                <br><br>
                The Fab Festival is a weekend event that follows the structure of the main event, but with activities, adapted for the general public and children, offered by Fab Labs around the world.<br>
                <ul>
                    <li>Interactive fair of inspiring projects</li>
                    <li>Conferences</li>
                    <li>Demonstrations</li>
                </ul>"
program: ""

---


<section class="no-padding" id="" style="padding: 25px 0px 50px 0px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <p class="{{ color_class }}">{{page.programdesc}}</p>
                <br>
                <div class="row no-gutter comingsoon text-center pad25 backwhite">
                        Detailed schedule coming soon!
                </div>
                <p class="{{ color_class }} text-center">{{page.program}}</p>
            </div>
        </div>
    </div>
</section>