---
layout: page
title: Program
lang: fr
prefix: fr/
permalink: /fr/programme/

title_text: "PROGRAMME"
subtitle_text: ""
color_class: "colorgreen"
img: ""

programdesc: "<strong>Participez à la plus grande conférence sur la fabrication numérique au monde.</strong>
            <br><br>
            Rejoignez-nous à Montréal, au Canada, pour la réunion annuelle du réseau international des Fab Labs !
            <br><br>
            Rejoignez les panélistes de classe mondiale, les membres de la communauté des Fab Labs, les fabricants du monde entier et les professionnels de tous les domaines à la 16e conférence annuelle des Fab Labs et au Fab Festival de Montréal, au Canada, du 27 juillet au 2 août, combiné avec le Fab City Summit, du 31 juillet au 2 août. Assistez à des panels de classe mondiale, démonstrations, ateliers, discussions et présentations de laboratoires par des représentants en provenance de tous les coins du globe !
            <br><br>
                <strong style='font-size:22px;'>Un aperçu du programme à venir : </strong>
                <br><br>
                Conférence sur l’état du réseau des Fab Labs et les impacts du mouvement des Fab Labs<br>
                Conférences prospectives<br>
                <ul>
                    <li>Les dernières méthodes de fabrication numérique et de machinage</li>
                    <li>Des approches innovantes au niveau des matériaux et de la matière</li>
                    <li>Conférenciers et panélistes sur le thème et les sujets du FAB16</li>
                </ul>
                Des ateliers “hands-on” et théoriques<br>
                Des groupes de travail<br>
                <ul>
                    <li>Instructeurs Academany</li>
                    <li>Assistive technologies</li>
                    <li>Humanitarian Lab</li>
                    <li>Scopes</li>
                    <li>Regional Networks</li>
                    <li>Education STEAM</li>
                    <li>Fab Labs with impact</li>
                    <li>Fab Kids</li>
                </ul>
                Des présentations de recherches<br>
                Des challenges<br>
                Des moments forts de réseautage<br>
                La cérémonie de diplômes Academany<br>
                L’exposition “7th Digital Fashion and Wearables”<br>
                Un SuperFabLab<br>
                Un vaste espace aménagé de tout l’appareillage d’un Fab Lab (et beaucoup plus encore)<br>
                Des Fab Labs mobiles<br>
                <br><br>
                <strong style='font-size:22px;'>Fab Festival - Les 1er et 2 août</strong>
                <br><br>
                Le Fab Festival est un événement se déroulant tout le weekend et qui reprend la structure de l’événement principal, mais avec des activités, adaptées pour le grand public et les enfants, offertes par les Fab Labs du monde entier.<br>
                <ul>
                    <li>Foire interactive de projets inspirants</li>
                    <li>Conférences</li>
                    <li>Démonstrations</li>
                </ul>"
program: ""

---

<section class="no-padding" id="" style="padding: 25px 0px 50px 0px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <p class="{{ color_class }}">{{page.programdesc}}</p>
                <br>
                <div class="row no-gutter comingsoon text-center pad25 backwhite">
                        Le calendrier détaillé arrive bientôt!
                </div>
                <p class="{{ color_class }} text-center">{{page.program}}</p>
            </div>
        </div>
    </div>
</section>


